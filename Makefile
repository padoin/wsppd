# Makefile
# File: "/home/fran/Dropbox/2011-clcar/Makefile"
# Created: "Sex, 07 Jan 2005 14:44:03 -0200 (kassick)"
# Updated: "Qua, 11 Mai 2011 15:50:35 -0300 (fran)"
# $Id: Makefile,v 1.3 2006/12/19 00:47:55 rvkassick Exp $
# Copyright (C) 2005, Rodrigo Virote Kassick <rvkassick@inf.ufrgs.br> 

BIBTEX = bibtex
MAINFILE = wsppd2014
OUTPUTDIR=output/
LATEX = pdflatex -halt-on-error -file-line-error -output-directory $(OUTPUTDIR)
#Listas de targets pdfs que devem ser gerados apartir dos fontes em outros formatos

rawimgs := $(wildcard images/*)

svgimgs := $(patsubst %.svg,%.pdf,$(wildcard svg/*.svg))

epsimgs := $(patsubst %.eps,%.pdf,$(wildcard eps/*.eps))

ooimgs := $(patsubst %.odg,%.pdf,$(wildcard ooffice/*.odg))

bibs    := $(wildcard biblio/*.bib)

sources := $(wildcard sections/*.tex)

SUBDIRS=svg eps ooffice

all: subdirs $(OUTPUTDIR)/$(MAINFILE).pdf

subdirs:
	@for i in $(SUBDIRS); do \
		echo "make all in $$i..."; \
		(cd $$i; $(MAKE) $(MFLAGS) $(MYMAKEFLAGS) all); done

view: $(OUTPUTDIR)/$(MAINFILE).pdf
	evince $(OUTPUTDIR)/$(MAINFILE).pdf &


comparative_table.tex: comparative.tex
	cat comparative.tex  > comparative_table.tex #| sed -e 's/\$$\\backslash\$$/\\/g' |sed -e 's/\\{/{/g' | sed -e 's/\\}/}/g' | iconv -f latin1 -t utf-8 >comparative_table.tex

#comparative.tex: comparative.gnumeric
#	ssconvert --export-type=Gnumeric_html:latex comparative.gnumeric


#$(MAINFILE).pdf: *.tex sections/*.tex *.sty *.bst $(bibs) $(rawimgs) $(svgimgs) $(epsimgs) $(ooimgs)

$(OUTPUTDIR)/$(MAINFILE).pdf: *.tex $(sources) *.bst $(styles) $(bibs) $(rawimgs) $(svgimgs) $(epsimgs) $(figimgs) $(gpiimgs) $(ooimgs)
	$(LATEX) $(MAINFILE)
	$(BIBTEX) $(OUTPUTDIR)/$(MAINFILE) || echo BIBTEX SUCKS
	$(LATEX) $(MAINFILE)
	$(LATEX) $(MAINFILE)

draft: *.tex $(bibs) $(rawimgs) $(svgimgs) $(epsimgs) $(figimgs) $(gpiimgs)
	$(LATEX) $(MAINFILE)



*.ps: *.pdf
	pdf2ps -dLanguageLevel=2 $(OUTPUTDIR)/$(MAINFILE).pdf -o $(OUTPUTDIR)/$(MAINFILE).ps




.PHONY: clean allclean

clean:
	rm -f output/* *.pdf *.ps *.bbl *.aux *.blg *.log *.lot *.lof *.idx || echo "Clean"


allclean:
	rm output/*
	rm -f *.pdf *.ps svg/*.pdf eps/*.pdf fig/*.pdf *.bbl *.aux *.blg *.log *.lot *.lof || echo "Clean"
	@for i in $(SUBDIRS); do \
		echo "make clean in $$i..."; \
		(cd $$i; $(MAKE) $(MFLAGS) $(MYMAKEFLAGS) clean);
	done

